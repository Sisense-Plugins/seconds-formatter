//	Initialize the copyWidget object
prism.run(["$filter", function($filter) {	

	//////////////////////////
	//	Plugin properties	//
	//////////////////////////

	//	Define allowed chart types
	var supportedCharts = ["chart/column","chart/line","chart/bar","chart/area"],
		supportedPivots = ["pivot"],
		supportedTables = ["tablewidget"],
		supportedIndicators = ["indicator"];
	
	//	Value Label options
    var labelName = "Time Formating";
	var labelProperty = 'timeFormatting';	
    

	//////////////////////////
	//	Utility Functions	//
	//////////////////////////

	//	Function to determine if this chart type is supported
	function chartIsSupported(type){
		if (supportedCharts.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	function pivotIsSupported(type){
		if (supportedPivots.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	function tableIsSupported(type){
		if (supportedTables.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	function indicatorIsSupported(type){
		if (supportedIndicators.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//	function to find the value items from a widget
	function getItems(widget,section) {
		var valueItems = $.grep(widget.metadata.panels, function(w){
			return w.title===section;
		})[0];
		return valueItems;
	}

	//	Function to get series by name
	function getSeries(name, widget){

		return widget.queryResult.series.filter(function(i){ 
			return i.name == name 
		})
	}

	//////////////////////////
	//	Formatter Functions	//
	//////////////////////////

	function dateFormatter(seconds){
		
		//	Create a moment object, assuming the value is seconds
		var m = moment("2000-01-01").seconds(seconds);

		//	Calculate the breakdown
		var t = {
			days: m.dayOfYear()-1,
			hours: m.hours(),
			minutes: m.minutes(),
			seconds: m.seconds()
		}

		//	Create a text string
		var valueText = '';
		if (t.days>0){
			valueText += t.days + ' days, ';
		}
		if (t.hours>0){
			valueText += ('00' + t.hours).slice(-2) + ':';
		}
		valueText += ('00' + t.minutes).slice(-2) + ':';
		valueText += ('00' + t.seconds).slice(-2) + '';

		return valueText;
	}

	function hcAxisFormatter(){
		return dateFormatter(this.value)
	}

	function hcDataLabelFormatter(point){
		return dateFormatter(this.y)
	}


	///////////////////////////
	//	Highcharts Functions //
	///////////////////////////

	function chartRenderOverride(widget, args){

		//	Loop through each metadata item, looking for time setting property
		widget.rawQueryResult.metadata.forEach(function(item){

			//	Do we need to use time formatting for this item?
			var useTimeFormatting = item[labelProperty];
			if (useTimeFormatting){

				//	Find any matching series
				var series = getSeries(item.jaql.title, widget)
				series.forEach(function(s){

					//	Assign the value label formatter
					s.dataLabels = {
						formatter: hcDataLabelFormatter
					}

					//	Assign the formatter to the relevant axis
					var y = widget.queryResult.yAxis[s.yAxis];
					if (y){
						y.labels.formatter = hcAxisFormatter;
					}
				})
			}
		})
	}

	function chartTooltipOverride(widget, args){

		//	Get the name of this series
		var thisSeriesName = args.context.pointScope.series.name;

		//	Look for a matching series in the metadata, but only if the property is set
		var matchingSeries = widget.rawQueryResult.metadata.filter(function(item){
			return (item[labelProperty]) && (item.jaql.title == thisSeriesName);
		})

		//	Good match, apply the business logic
		if (matchingSeries.length > 0) {
			
			//	Get the formatted value text
			formattedValue = dateFormatter(args.context.pointScope.options.y);

			//	Loop through all data points, and set the text for tooltip
			args.context.points.forEach(function(point){
				point.value = formattedValue
			})
		}
	}

	function indicatorRenderOverride(widget,args){
		
		//	Loop through each metadata item, looking for time setting property
		widget.rawQueryResult.metadata.forEach(function(item, index){

			//	Do we need to use time formatting for this item?
			var useTimeFormatting = item[labelProperty];
			if (useTimeFormatting){

				var x = widget.queryResult[item.source];
				if (x){
					x.text = dateFormatter(x.data);
				}
			}
		})
	}

	function tableRenderOverride(widget,args){

		var changeValues = function(widget, element){

			//	Loop through each metadata item, looking for time setting property
			widget.rawQueryResult.metadata.forEach(function(item, index){

				//	Do we need to use time formatting for this item?
				var useTimeFormatting = item[labelProperty];
				if (useTimeFormatting){

					//	Loop through each row, finding the right column
					$('tbody > tr[role="row"] > td:nth-child(' + (index+1) +  ')').each(function(cell){

						// Parse the value
						var value = parseFloat($(this).text().replace(',','').replace('$',''));

						//	Write to the cell
						$(this).text( dateFormatter(value) );
					})
				}
			})
		}

		//  Define the fancy table handler
	    var tableHandler = function(e,settings,json){
	        changeValues(widget,widgetEl);
	    }

	    //	Get the div container, to scope jquery selectors
		var widgetEl = prism.$ngscope.appstate == "widget" ? $('.widget-body') : $('widget[widgetid="' + widget.oid + '"]');

	    //  Get a reference to the datatable object(s)
	    var myTables = $('table#grid',widgetEl);

	    //  Remove old handlers for tables that are going to be removed from dom
	    myTables.off('draw.dt',tableHandler);

	    //  Find the newest one
	    var newTable = $(myTables[myTables.length-1]).dataTable();  

	    //  Add event handler to handle redraws, paging, etc
	    newTable.on('draw.dt',tableHandler);

	    //	Run the first formatter
	    changeValues(widget,widgetEl);
		
	}

	function pivotRenderOverride(widget, args){

		//	Get the div container, to scope jquery selectors
		var widgetEl = prism.$ngscope.appstate == "widget" ? $('.widget-body') : $('widget[widgetid="' + widget.oid + '"]');

		//	Loop through each metadata item, looking for time setting property
		widget.metadata.panel('values').items.forEach(function(item, index){

			//	Do we need to use time formatting for this item?
			var useTimeFormatting = item[labelProperty];
			if (useTimeFormatting){

				//	Apply the text formatting for each cell in the table
				$('td.p-value[fidx=' + item.field.index + ']',widgetEl).each(function(i){
					$(this).text( dateFormatter( parseFloat( $(this).attr('val') ) ) );
				})	
			}
		})
	}

    //////////////////////////////////
    //  Initialization Functions    //
    //////////////////////////////////

	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		args.widget.on("destroyed", onWidgetDestroyed);
		//	Add hook on render
		if (chartIsSupported(args.widget.type)) {
			//	Highcharts event handlers
			args.widget.off("render", chartRenderOverride);
			args.widget.off("beforedatapointtooltip", chartTooltipOverride);
			args.widget.on("render", chartRenderOverride);
			args.widget.on("beforedatapointtooltip", chartTooltipOverride);
		} else if (pivotIsSupported(args.widget.type)) {
			//	pivot event handlers
			args.widget.off("domready", pivotRenderOverride);
			args.widget.on("domready", pivotRenderOverride);
		} else if (tableIsSupported(args.widget.type)) {
			//	table event handlers
			args.widget.off("domready", tableRenderOverride);
			args.widget.on("domready", tableRenderOverride);
		} else if (indicatorIsSupported(args.widget.type)) {
			//	Indicator event handlers
			args.widget.off("render", indicatorRenderOverride);
			args.widget.on("render", indicatorRenderOverride);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}


    //////////////////////////////////
    //  Menu  Functions             //
    //////////////////////////////////

    //  Function to make sure this is a settings menu for a measure
    function isValueMenu(settings) {
        
        //  Try to evaluate
        try {
            // Widget must be an allowed chart
            var supported = chartIsSupported(prism.$ngscope.widget.type) || pivotIsSupported(prism.$ngscope.widget.type) || tableIsSupported(prism.$ngscope.widget.type) || indicatorIsSupported(prism.$ngscope.widget.type)  ;       
            if(!supported){
                return false;
            }
            //  Check the menu's name
            if (settings.name !== "widget-metadataitem"){
                return false;
            }

            //  Check the jaql (except for table widget)
            if (prism.$ngscope.widget.type !== "tablewidget") {
	            if (!settings.jaql.agg && !settings.jaql.formula) {
	                return false;
	            }
	        }
        } catch(e) {
            return false;
        }
        return true;
    }

	//	Create Options for the menu
	prism.on("beforemenu", function (e, args) {
        
        //  Should the new menu item be added?
        var shouldAdd = isValueMenu(args.settings);
        if (shouldAdd) {
                
            //  Get the widget and the panel
            var panelItem = args.settings.item;

            //  Make sure there is a settings object  
            if (!panelItem[labelProperty]){
                panelItem[labelProperty] = false;
            }            
            
            //  Get the list of existing items            
            var items = args.settings.items;

            //  Create a separator
            var separator = {
                type: "separator"
            };

            //  Function the runs when an item is picked
            var itemPicked = function(){
                
                //  toggle the setting
                this.settings[labelProperty] = !this.checked;
            
                //  Redraw the widget                
                prism.activeWidget.redraw();
            }

            //  Create menu for picking the regression measure
            var labelTypes = {
                caption: labelName,
                checked: panelItem[labelProperty],
                size: 'xl',
                type: 'check',
                settings: panelItem,
                execute: itemPicked,
                closing: true
            };

            //   Add options to the menu         
            args.settings.items.push(separator);
            args.settings.items.push(labelTypes);
        }        
	});
}])